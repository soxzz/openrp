global.GetCurrentResourceName ||= () => "current_resource_name";
global.onNet ||= (...args: any[]) => {
  console.log("ON NET MOCK", args);
};
global.emitNet ||= (...args: any[]) => {
  console.log("EMIT NET MOCK", args);
};

global.RegisterCommand ||= (...args: any[]) => {
  console.log("REGISTER COMMAND MOCK", args);
};

global.emit ||= (...args: any[]) => {
  console.log("EMIT MOCK", args);
};

global.on ||= (...args: any[]) => {
  console.log("ON MOCK", args);
};

global.exports ||= (...args: any[]) => {
  console.log("EXPORTS MOCK", args);
};

// @ts-ignore
global.setTick ||= (...args: any[]) => {
  console.log("SET TICK MOCK", args);
};
