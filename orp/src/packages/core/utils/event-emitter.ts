import { EventEmitter2 } from "eventemitter2";
import container from "../container";

export const eventEmitter = new EventEmitter2();

container.bind(EventEmitter2).toDynamicValue(() => eventEmitter);
