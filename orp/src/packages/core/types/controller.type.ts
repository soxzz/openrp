export type controller<T = any> = {
  new (...args: any[]): T;
};
