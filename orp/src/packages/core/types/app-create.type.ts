import { controller as Controller, AppOptions } from "./";

export type AppCreate = {
  controllers?: Controller[];
  options?: AppOptions;
};
