export type chatCommand = {
  command: string;
  propertyKey: string | symbol;
  description?: string;
  params?: any;
};
