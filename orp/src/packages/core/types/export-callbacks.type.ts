export type ExportCallbacks = {
  resource: string;
  method: string;
  args: any[];
  propertyKey: string | symbol;
};
