import { CHAT_COMMAND, TUNNEL } from "../const";

export type ExecContext = {
  type: typeof TUNNEL | typeof CHAT_COMMAND;
  session: Record<string, any>;
  args: any[];
  source: string;
};
