export type Ticks = {
  interval: number;
  propertyKey: string | symbol;
};
