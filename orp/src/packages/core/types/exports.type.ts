export type Exports = {
  name: string;
  propertyKey: string | symbol;
};
