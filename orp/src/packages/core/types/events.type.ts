export type Events = {
  name: string;
  propertyKey: string | symbol;
};
