import { Container } from "inversify";
import container from "../container";
import {
  CHAT_COMMAND,
  CONTROLLER,
  EVENT,
  EXPORT,
  EXPORT_CALLBACK,
  GUARD,
  LOCAL_EVENT,
  NET_EVENT,
  PARAMETER,
  RESOURCE_NAME,
  TICK,
  TUNNEL,
} from "../const";
import { extractMetadata } from "../decorators/helpers";
import { IGuard } from "../interfaces";
import {
  controller as Controller,
  AppOptions,
  AppCreate,
  TunnelEvents,
  chatCommand as ChatCommand,
  ExecContext,
  Events,
  Exports,
  Ticks,
} from "../types";
import { eventEmitter } from "../utils";
import { ExportCallbacks } from "../types/export-callbacks.type";

export class App {
  private controllers: Set<Controller> = new Set();
  private options: AppOptions;
  private container: Container;

  public static create(arg: AppCreate): App {
    const app = new App();
    arg.controllers?.forEach((controller: Controller) =>
      app.controllers.add(controller)
    );
    app.options = arg.options || null;
    return app;
  }

  public async start(): Promise<void> {
    this.container = container;
    if (!this.container) throw new Error("[ORP] Container is not set");
    for (const controllerKey of this.controllers.keys()) {
      const controller: Controller = this.container.get(controllerKey);
      this.initTunnelEvents(controller);
      this.initChatCommands(controller);
      this.initEvents(controller);
      this.initExports(controller);
      this.initLocalEvents(controller);
      this.initNetEvents(controller);
      this.initTicks(controller);
      this.initExportCallbacks(controller);
    }
    console.log("[ORP][Core]APP Started");
  }

  private processGuard = (g: any): IGuard => {
    if (typeof g === "function") {
      if (g?.prototype?.canAccess) {
        return this.container.get(g);
      } else {
        return { canAccess: g };
      }
    }
  };

  private getControllerName = (c: Controller): string => {
    const name = Reflect.getMetadata(CONTROLLER, c);
    if (!name)
      throw new Error(`${c.constructor} is not decorated as Controller`);
    return name;
  };

  private initEvents(controller: Controller): void {
    const metadata: Events[] = extractMetadata(EVENT, controller) || [];
    metadata.forEach(({ name, propertyKey }) => {
      on(name, (...args: any[]) => controller[propertyKey](...args));
    });
  }

  private initExports(controller: Controller): void {
    const metadata: Exports[] = extractMetadata(EXPORT, controller) || [];
    metadata.forEach(({ name, propertyKey }) => {
      global.exports(name, (...args: any[]) =>
        controller[propertyKey](...args)
      );
    });
  }

  private initLocalEvents(controller: Controller): void {
    const metadata: Events[] = extractMetadata(LOCAL_EVENT, controller) || [];
    metadata.forEach(({ name, propertyKey }) => {
      eventEmitter.on(name, (...args: any[]) =>
        controller[propertyKey](...args)
      );
    });
  }

  private initNetEvents(controller: Controller): void {
    const metadata: Events[] = extractMetadata(NET_EVENT, controller) || [];
    metadata.forEach(({ name, propertyKey }) => {
      onNet(name, (...args: any[]) => controller[propertyKey](...args));
    });
  }

  private initTicks(controller: Controller): void {
    const metadata: Ticks[] = extractMetadata(TICK, controller) || [];
    metadata.forEach(({ interval, propertyKey }) => {
      const method = controller[propertyKey].bind(controller);
      if (interval === 0) {
        setTick(method);
      } else {
        setTick(async () => {
          await new Promise((resolve) => setTimeout(resolve, interval));
          await method();
        });
      }
    });
  }

  private initTunnelEvents(controller: Controller): void {
    const controllerName = this.getControllerName(controller);
    const tunnels: TunnelEvents[] = extractMetadata(TUNNEL, controller) || [];
    const tunnel: Record<string, (context: ExecContext) => unknown> = {};
    const Context = (args: any[]): ExecContext => ({
      args,
      session: {},
      type: TUNNEL,
      source: args[0]?.toString(),
    });
    tunnels.forEach(({ propertyKey }) => {
      const guards: IGuard[] = Reflect.getMetadata(
        GUARD,
        controller,
        propertyKey
      )?.map(this.processGuard);
      const parameters: ((fn: ExecContext) => unknown)[] = Reflect.getMetadata(
        PARAMETER,
        controller,
        propertyKey
      );

      tunnel[propertyKey as string] = async (context: ExecContext) => {
        if (guards) {
          await guards.forEach(async (guard: IGuard) => {
            if (!(await guard.canAccess(context))) {
              return;
            }
          });
        }
        if (parameters) {
          for (let i = 0; i < parameters.length; i++) {
            const result = await parameters[i](context);
            if (!result) {
              return;
            }
            context.args.splice(i, 0, result);
          }
        }
        await controller[propertyKey](...context.args);
      };
    });
    this.createTunnel(controllerName, Context, tunnel);
  }

  private initExportCallbacks(controller: Controller): void {
    const metadata: ExportCallbacks[] =
      extractMetadata(EXPORT_CALLBACK, controller) || [];
    metadata.forEach(({ resource, method, args, propertyKey }) => {
      global.exports[resource][method]((...params: any[]) => {
        controller[propertyKey](...params);
      }, ...args);
    });
  }

  private createTunnel(
    controllerName: string,
    Context: Function,
    tunnel: Record<string, (context: ExecContext) => unknown>
  ): void {
    onNet(
      `${RESOURCE_NAME}:${controllerName}:tunnel-request`,
      async (method: string, id: number, ...args: any[]) => {
        const context: ExecContext = Context(args);
        context.session.method = method;
        const cb = tunnel[method];
        const res = cb ? await cb(context) : null;
        emitNet("tunnel-response", context.source, id, res);
      }
    );
    onNet(
      `${RESOURCE_NAME}:${controllerName}:tunnel`,
      async (method: string, ...args: any[]) => {
        const context: ExecContext = Context(args);
        context.session.method = method;
        const cb = tunnel[method];
        cb && (await cb(context));
      }
    );
  }

  private initChatCommands(controller: Controller): void {
    const commands: ChatCommand[] =
      extractMetadata(CHAT_COMMAND, controller) || [];
    commands.forEach((chatCommand: ChatCommand) => {
      const { command, propertyKey, description, params } = chatCommand;
      const guards: IGuard[] = Reflect.getMetadata(
        GUARD,
        controller,
        propertyKey
      )?.map(this.processGuard);
      const parameters: ((fn: ExecContext) => unknown)[] = Reflect.getMetadata(
        PARAMETER,
        controller,
        propertyKey
      );
      const Context = (args: any): ExecContext => ({
        args,
        session: {},
        type: CHAT_COMMAND,
        source: args[0]?.toString(),
      });

      RegisterCommand(
        command,
        async (...args: any[]) => {
          const context = Context(args);
          context.session.command = command;
          if (guards) {
            await guards.forEach(async (guard: IGuard) => {
              if (!(await guard.canAccess(context))) {
                return;
              }
            });
          }
          if (parameters) {
            for (let i = 0; i < parameters.length; i++) {
              const res = await parameters[i](context);
              if (!res) {
                return;
              }
              context.args.splice(i, 0, res);
            }
          }

          await controller[propertyKey](...context.args);
        },
        false
      );

      if (description || params) {
        emit("chat:addSuggestion", `/${command}`, description, params);
      }
    });
  }
}
