import { CONTROLLER } from "../../const";
import { Injectable } from "../di/injectable.decorator";

export const Controller =
  (name?: string): ClassDecorator =>
  (target) => {
    name ||= target.name;
    Reflect.defineMetadata(CONTROLLER, name, target.prototype);
    Reflect.decorate([Injectable()], target);
  };
