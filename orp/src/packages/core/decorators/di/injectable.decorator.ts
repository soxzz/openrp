import { injectable } from "inversify";
import container from "../../container";

export const Injectable =
  (token?: string): ClassDecorator =>
  (target) => {
    Reflect.decorate([injectable()], target);
    container
      .bind(token || target)
      .to(target as any)
      .inSingletonScope();
  };
