import { PARAMETER } from "../../const";
import { ExecContext } from "../../types";

export const createNetEventParameter =
  (fn: (context: ExecContext) => unknown): ParameterDecorator =>
  (target, propertyKey, parameterIndex) => {
    const metadata = Reflect.getMetadata(PARAMETER, target, propertyKey) || [];
    metadata[parameterIndex] = fn;
    Reflect.defineMetadata(PARAMETER, metadata, target, propertyKey);
  };
