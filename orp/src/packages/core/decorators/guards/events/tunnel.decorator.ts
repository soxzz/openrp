import { TUNNEL } from "../../const";
import { extendArrayMetadata } from "../helpers";

export const Tunnel = (): MethodDecorator => (target, propertyKey) => {
  extendArrayMetadata(TUNNEL, target, { propertyKey });
};
