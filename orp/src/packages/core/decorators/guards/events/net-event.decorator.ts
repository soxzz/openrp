import { extendArrayMetadata } from "../helpers";
import { NET_EVENT } from "../../const";

export const NetEvent =
  (name: string): MethodDecorator =>
  (target, propertyKey) => {
    extendArrayMetadata(NET_EVENT, target, { name, propertyKey });
  };
