import { extendArrayMetadata } from "../helpers";
import { LOCAL_EVENT } from "../../const";

export const LocalEvent =
  (name: string): MethodDecorator =>
  (target, propertyKey) => {
    extendArrayMetadata(LOCAL_EVENT, target, { name, propertyKey });
  };
