import { extendArrayMetadata } from "../helpers";
import { CHAT_COMMAND } from "../../const/metadata-keys.constant";

export const ChatCommand =
  (
    command: string,
    description?: string,
    params?: [{ name: string; help: string }]
  ): MethodDecorator =>
  (target, propertyKey) => {
    extendArrayMetadata(CHAT_COMMAND, target, {
      command,
      propertyKey,
      description,
      params,
    });
  };
