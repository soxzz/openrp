import { extendArrayMetadata } from "../helpers";
import { EXPORT_CALLBACK } from "../../const";

export const ExportCallback =
  (resource: string, method: string, ...args: any[]): MethodDecorator =>
  (target, propertyKey) => {
    extendArrayMetadata(EXPORT_CALLBACK, target, {
      resource,
      method,
      args,
      propertyKey,
    });
  };
