import { extendArrayMetadata } from "../helpers";
import { EVENT } from "../../const";

export const Event =
  (name: string): MethodDecorator =>
  (target, propertyKey) => {
    extendArrayMetadata(EVENT, target, { name, propertyKey });
  };
