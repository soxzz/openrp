import { extendArrayMetadata } from "../helpers";
import { EXPORT } from "../../const";

export const Export =
  (name: string): MethodDecorator =>
  (target, propertyKey) => {
    extendArrayMetadata(EXPORT, target, { name, propertyKey });
  };
