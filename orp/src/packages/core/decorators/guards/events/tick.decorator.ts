import { extendArrayMetadata } from "../helpers";
import { TICK } from "../../const";

export const Tick =
  (interval: number = 0): MethodDecorator =>
  (target, propertyKey) => {
    if (interval < 0) {
      throw new Error(
        `[ORP] ${propertyKey as string} - Tick interval must be positive value`
      );
    }
    extendArrayMetadata(TICK, target, { interval, propertyKey });
  };
