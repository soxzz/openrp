import { extendArrayMetadata } from "../helpers";
import { GUARD } from "../../const";
import { IGuard } from "../../interfaces";
import { controller as Controller } from "../../types";

export const UseGuard =
  (guard: IGuard["canAccess"] | IGuard | Controller<IGuard>): MethodDecorator =>
  (target, propertyKey) => {
    extendArrayMetadata(GUARD, target, guard, propertyKey);
  };
