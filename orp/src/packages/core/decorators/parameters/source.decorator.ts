import { createNetEventParameter } from "../helpers";
import { ExecContext } from "../../types";

const getSource = (context: ExecContext) => {
  return context.source;
};

export const Source = () => createNetEventParameter(getSource);
