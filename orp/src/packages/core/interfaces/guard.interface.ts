import { ExecContext } from "../types";

export interface IGuard {
  canAccess(context: ExecContext): boolean | Promise<boolean>;
}
