import { CONTAINER } from "../const";
import { Container } from "inversify";

const getContainer = (): Container => {
  const c: Container = Reflect.getMetadata(CONTAINER, global);
  return c ? c : createContainer();
};

const createContainer = (): Container => {
  const c: Container = new Container({
    skipBaseClassChecks: true,
    autoBindInjectable: true,
    defaultScope: "Singleton",
  });
  Reflect.defineMetadata(CONTAINER, c, global);
  return c;
};

const container = getContainer();

export default container;
