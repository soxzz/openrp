# Welcome to OpenRP

<img src="readme/openrp-logo-min.png"  width="50%" height="50%">

> Open RP is a typescript open source framework for fiveM
It aims to provide an extensible base in order to develop other mod

## Requirements 

[![Node version](https://img.shields.io/badge/node%40lts-%3E%3D14.16.1-brightgreen?style=for-the-badge&logo=Node.js)](https://nodejs.org/en/)

## Installation Setup

npm package to come for now you could import ORP folder that contain the core of the projet, build it and do a npm link in your ORP-module


## How it works

You should declare an Application:

```typescript
import { App } from "orp/dist";
#import your controllers here

const server = App.create({
  controllers: [...AllControllers],
});

server.start();
```

On start it will inject all dependencies of your controller in the application, you could do it for client and server side.

```mermaid
graph TD
    A[Application] -->|start| B(Inject all Controllers)
    B --> C{Controllers}
    C --> D[Ticks]
    C --> E[AllEvents]
    E --> F[TunnelEvents]
    E --> G[Events]
    E --> H[LocalEvents]
    E --> I[NetEvents]
    C --> J[AllExports]
    J --> K[Exports]
    J --> L[ExportCallbacks]
    C --> M[ChatCommands]
```

## How to write a Controller
