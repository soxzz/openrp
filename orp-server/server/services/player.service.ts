export class PlayerService {
  getLicenseIdentifier(source: string) {
    for (let i = 0; i < GetNumPlayerIdentifiers(source); i++) {
      const identifier = GetPlayerIdentifier(source, i);
      if (identifier.includes("license:")) {
        return identifier;
      }
    }
  }
}
