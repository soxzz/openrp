import { App } from "orp-framework/dist";
import { ConnectionController } from "./controllers";
const server = App.create({
  controllers: [ConnectionController],
});

server.start();
