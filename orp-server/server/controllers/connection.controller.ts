import { Controller, Event, Source } from "orp-framework/dist";
import { PlayerService } from "../services/player.service";

@Controller("ConnectionController")
export class ConnectionController {
  private readonly playerService: PlayerService = new PlayerService();
  @Event("playerConnecting")
  async playerConnecting(name: string, setKickReason: any, deferrals: any) {
    deferrals.defer();
    const source = String(global.source);
    deferrals.update(`WELCOME ${name}`);
    await setTimeout(() => {}, 2000);
    const identifier = this.playerService.getLicenseIdentifier(source);
    console.log(identifier);
    if (!identifier) {
      deferrals.done("NO LICENSE");
    } else if (identifier.includes("HTTP 503")) {
      deferrals.done("RELAUNCH");
    } else {
      deferrals.done();
    }
    await setTimeout(() => {}, 1000);
  }
}
