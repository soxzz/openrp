fx_version 'adamant'
game 'common'

name 'orp-sample-typescript'
description 'OpenRP sample typescript'
author 'AnoterFxcker'
url ''

resource_type 'gametype' { name = 'Freeroam' }

client_script 'dist/client.js'
server_script 'dist/server.js'
