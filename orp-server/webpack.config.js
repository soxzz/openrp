const webpack = require("webpack");
const glob = require("glob");
const path = require("path");
const FilterWarningsPlugin = require("webpack-filter-warnings-plugin");
const buildPath = path.resolve(__dirname, "dist");

const server = {
  entry: "./server/index.ts",
  mode: "development",
  module: {
    rules: [
      {
        test: /\.tsx?$/,
        use: ["ts-loader", "eslint-loader"],
        exclude: /node_modules/,
      },
    ],
  },
  externals: {
    mysql2: "commonjs mysql2",
    typeorm: "commonjs typeorm",
  },
  plugins: [
    new webpack.DefinePlugin({ "global.GENTLY": false }),
    new FilterWarningsPlugin({
      exclude: [
        /mongodb/,
        /mssql/,
        /mysql/,
        /oracledb/,
        /pg/,
        /pg-native/,
        /pg-query-stream/,
        /react-native-sqlite-storage/,
        /redis/,
        /sqlite3/,
        /sql.js/,
        /typeorm-aurora-data-api-driver/,
      ],
    }),
  ],
  node: {
    __dirname: false,
    __filename: false,
  },
  optimization: {
    minimize: true,
  },
  resolve: {
    extensions: [".tsx", ".ts", ".js"],
  },
  output: {
    filename: "server.js",
    path: buildPath,
  },
  target: "electron-main",
};

const client = {
  entry: "./client/index.ts",
  module: {
    rules: [
      {
        test: /\.tsx?$/,
        use: ["ts-loader", "eslint-loader"],
        exclude: /node_modules/,
      },
    ],
  },
  optimization: {
    minimize: true,
  },
  resolve: {
    extensions: [".tsx", ".ts", ".js"],
  },
  output: {
    filename: "client.js",
    path: buildPath,
  },
};

module.exports = [server, client];
